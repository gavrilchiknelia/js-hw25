$(function() {
    let slider = $('.slider'),
        slideWidth = $('.slider-box').outerWidth(),
        slideCount = $('.slider img').length,
        prev = $('.slider-box .prev'),
        next = $('.slider-box .next'),
        animateTime = 1000,
        course = 1,                                         // Направление движения слайдера (1 или -1)
        margin = - slideWidth;                              // Первоначальное смещение слайдов

    $('.slider img:last').clone().prependTo('.slider');   // Копия последнего слайда помещается в начало.
    $('.slider img').eq(1).clone().appendTo('.slider');   // Копия первого слайда помещается в конец.
    $('.slider').css('margin-left', -slideWidth);         // Контейнер .slider сдвигается влево на ширину одного слайда.

    function animate(){
        if (margin==-slideCount*slideWidth-slideWidth){     // Если слайдер дошел до конца
            slider.css({'marginLeft':-slideWidth});           // то блок .slider возвращается в начальное положение
            margin=-slideWidth*2;
        }else if(margin==0 && course==-1){                  // Если слайдер находится в начале и нажата кнопка "назад"
            slider.css({'marginLeft':-slideWidth*slideCount});// то блок .slider перемещается в конечное положение
            margin=-slideWidth*slideCount+slideWidth;
        }else{                                              // Если условия выше не сработали,
            margin = margin - slideWidth*(course);              // значение margin устанавливается для показа следующего слайда
        }
        slider.animate({'marginLeft':margin},animateTime);  // Блок .slider смещается влево на 1 слайд.
    }

    prev.click(function() {                               // Нажата кнопка "назад"
        if (slider.is(':animated')) { return false; }       // Если не происходит анимация
        let course2 = course;                               // Временная переменная для хранения значения course
        course = -1;                                        // Устанавливается направление слайдера справа налево
        animate();                                          // Вызов функции animate()
        course = course2 ;                                  // Переменная course принимает первоначальное значение
    });
    next.click(function() {                               // Нажата кнопка "назад"
        if (slider.is(':animated')) { return false; }       // Если не происходит анимация
        let course2 = course;                               // Временная переменная для хранения значения course
        course = 1;                                         // Устанавливается направление слайдера справа налево
        animate();                                          // Вызов функции animate()
        course = course2 ;                                  // Переменная course принимает первоначальное значение
    });

});